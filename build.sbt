androidBuild

lazy val commonSettings = Seq(
  organization := "com.gmail.naetmul",
  version := "0.1.0",
  scalaVersion := "2.11.8"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    name := "AndroidScala"
  )

// Android 6.0 API Level 23
platformTarget in Android := "android-23"

javacOptions in Compile ++= "-source" :: "1.7" :: "-target" :: "1.7" :: Nil
