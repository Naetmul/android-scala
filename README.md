# ReadMe

This project uses GitHub's [scala-android/sbt-android (Build Android Projects Using SBT)](https://github.com/scala-android/sbt-android) project.

## Prerequisites
* JDK 1.6+ (1.8 recommended)
* Scala 2.11.x
* SBT 0.13.8+
* Android SDK
* (OPTIONAL) IntelliJ IDEA with Scala and SBT plugins

## How to Set Up a New Project
1. Set the environment variable `ANDROID_HOME` pointing to the path where the Android SDK is installed. If `ANDROID_HOME` is not set, an Android SDK will be installed automatically at `~/.android/sbt/sdk`. If any components are missing from your SDK, they will be installed automatically.
    * (OPTIONAL) Set `ANDROID_NDK_HOME` if NDK building is desired and an NDK already installed. If neither are set, or an NDK is not installed, an NDK will be installed to `~/.android/sbt/sdk/ndk-bundle` automatically if an NDK build is detected (`Android.mk` and friends)
1. Create a directory named `project` within your project and add the file `project/plugins.sbt`, in it, add the following line:  
    ```
    addSbtPlugin("org.scala-android" % "sbt-android" % "1.6.12")
    ```
1. Run `sbt` in your project's root directory and create a new android project using `gen-android` in SBT.
    * Instead of creating a new project, one can also do `sbt gen-android-sbt` to make sure everything is properly setup in an existing project.
    * Usage: `gen-android` <platform-target> <package-name> <name>
        - <platform-target>: See [Android API Level](https://developer.android.com/guide/topics/manifest/uses-sdk-element.html#ApiLevels). You need to prepend `android-` to the number.
            * `android-23`: Android 6.0
            * `android-21`: Android 5.0
            * `android-19`: Android 4.4
        - <package-name>: Package name (Java or Scala)
        - <name>: Project name
        - example) `gen-android android-23 com.gmail.naetmul.androidscala AndroidScala`
1. Confirm the root directory's `build.sbt` contains `androidBuild`, `platformTarget in Android := "android-xx"`, and Scala version (2.11.8) setting.